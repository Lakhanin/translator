const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const translateText = require('./translate')
const bodyParser = require('body-parser')

//app config
const app = express()
const port = process.env.PORT || 8080

//middleware
app.use(express.json())
app.use(cors())
app.use(bodyParser.json(
    { limit: '50mb', extended: true }
  ))

//DB config
const connection_url= 'mongodb://root:bqt6JjvtVytsvnzH@database:27017/Translator'
const db = mongoose.connection

db.once('open', ()=> {
    console.log('DB is connected')
})

async function start(){
    try{
        await mongoose.connect(connection_url, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        }) 
        //api routes
        app.get('/', (req, res) => res.status(200).send('Hello world'))
        app.post('/translate', async (req, res) => {
            res.status(200).send(await translateText(req))
        })

        //listen
        app.listen(port, () => console.log(`Listening on localhost:${port}`))

            } catch(e) {
        console.log('Server Error', e.message)
        process.exit(1)
    }
}

start()



        //TODO /app.translateAuto
        // /word.save {word, dictionaryName}
        // /word.delete {word, dictionaryName}
        // /history
        // /dictionary.list
        // /dictionary.save
        // /dictionary.delete
        // /dictionary.update
        // /profile
        // /profile.save
        // /profile.update
        // /
