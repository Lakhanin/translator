const {Router} = require('express')
const router = Router()
const projectId = 'translator-295319'
const {Translate} = require('@google-cloud/translate').v2;

// Creates a client
const translate = new Translate({projectId});


 
 module.exports = async function translateText(req) {
     console.log(req.body)
try{
 //  The text to translate, e.g.
 const text = req.body.word;
//  The target language, e.g.
 const target = req.body.lang;


//  const {
//      text,
//      lang
//  } = req.body

 let [translations] = await translate.translate(text, target);
 translations = Array.isArray(translations) ? translations : [translations];
 console.log('Translations:');
 translations.forEach((translation, i) => {
  console.log(`${text[i]} => (${target}) ${translation}`);
 });
 return translations

}catch(e){
 return e.message
}
}