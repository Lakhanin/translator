const {Schema, model, Types} = require('mongoose')

const schema = new Schema ({
    code: {type:String, required: true},
    date: {type: Date, default: Date.now},
    owner: {type: Types.ObjectId, ref: 'Words'}
})

module.exports = model('User', schema)