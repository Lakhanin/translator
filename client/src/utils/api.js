import fetch from "isomorphic-fetch";
import querystring from "querystring";
import { APIError } from "./apierror";

const checkStatus = resolve => response => {
    if (response.status >= 200 && response.status < 300) {
        return resolve(response.json());
    }

    return new Promise((_, reject) => {
        response
            .json()
            .then(body => {
                reject({
                    status: response.status,
                    body
                });
            })
            .catch(() => {
                reject({ status: response.status });
            });
    });
};

export class Api {
    constructor(endpoint) {
        this.endpoint = endpoint || "http://localhost:8080"; // TODO fix this shit
    }

    _createHeader(token) {
        let headers = {
            "Content-Type": "application/json"
        };

        if (token) {
            headers = {
                ...headers
                // Authorization: `Bearer ${token}`
            };
        }

        return headers;
    }

    _call(url, options, queryParams) {
        return new Promise((resolve, reject) => {
            if (queryParams) {
                url = `${url}?${querystring.stringify(queryParams)}`;
            }

            fetch(url, options)
                .then(checkStatus(resolve))
                .catch(({ status, body }) => {
                    let err,
                        errorMessage = "";

                    switch (status) {
                        case 404:
                            errorMessage = "The API could not be reached.";
                            break;
                        case 401:
                        case 402:
                        case 403:
                            errorMessage =
                                "Authorization error occurred. Please sign out and log back in.";
                            break;
                        case 400:
                            errorMessage = "An invalid API call was made";
                            break;
                        default:
                            errorMessage =
                                "A technical issue occurred inside our API.";
                            break;
                    }

                    switch (options.method) {
                        case "PUT": {
                            err = new APIError({
                                message: `Failed to update data: ${errorMessage}`,
                                status,
                                body
                            });
                            break;
                        }
                        case "POST": {
                            err = new APIError({
                                message: `Failed to save data: ${errorMessage}`,
                                status,
                                body
                            });
                            break;
                        }
                        case "DELETE": {
                            err = new APIError({
                                message: `Failed to delete data: ${errorMessage}`,
                                status,
                                body
                            });
                            break;
                        }
                        default: {
                            err = new APIError({
                                message: `Failed to call the API: ${errorMessage}`,
                                status,
                                body
                            });
                            break;
                        }
                    }
                    console.error("API error:", err);
                    reject(err);
                });
        });
    }

    get(path, token, queryParams) {
        const url = `${this.endpoint}/${path}`,
            headers = this._createHeader(token);

        return this._call(url, { headers }, queryParams);
    }

    delete(path, token, payload) {
        const url = `${this.endpoint}/${path}`,
            headers = this._createHeader(token);

        const options = {
            headers: headers,
            method: "DELETE",
            body: JSON.stringify(payload || {})
        };

        return this._call(url, options);
    }

    put(path, token, payload) {
        return this.post(path, token, payload, "PUT");
    }

    post(path, token, payload, method = "POST") {
        const url = `${this.endpoint}/${path}`,
            headers = this._createHeader(token);

        const options = {
            headers: headers,
            method,
            body: JSON.stringify(payload || {})
        };

        return this._call(url, options);
    }

    downloadJSON(path, token, queryParams) {
        return this.download(path, token, queryParams, {
            contentType: "plain/text",
            extension: ".json"
        });
    }

    downloadCSV(path, token, queryParams) {
        return this.download(path, token, queryParams, {
            contentType: "plain/text",
            extension: ".csv"
        });
    }

    downloadZIP(path, token, queryParams) {
        return this.download(path, token, queryParams, {
            contentType: "application/octet-stream",
            extension: ".zip"
        });
    }

    download(path, token, queryParams, opts) {
        let url = `${this.endpoint}/${path}`;

        const headers = this._createHeader(token);
        headers["Content-Type"] = "*";

        return new Promise((resolve, reject) => {
            if (queryParams) {
                url = `${url}?${querystring.stringify(queryParams)}`;
            }

            const options = {
                headers: headers,
                method: "GET"
            };

            fetch(url, options)
                .then(result => result.blob())
                .then(result => {
                    const a = document.createElement("a");
                    document.body.appendChild(a);
                    a.style = "display: none";
                    const blob = new Blob([result], { type: opts.contentType });

                    const url = window.URL.createObjectURL(blob);
                    a.href = url;

                    if (queryParams && queryParams.organisationName) {
                        a.download = `export-${queryParams.organisationName
                            .toLowerCase()
                            .trim()
                            .replace(/\s/gi, "-")}-${new Date().getTime()}${
                            opts.extension
                        }`;
                    } else {
                        a.download = `export-${new Date().getTime()}${
                            opts.extension
                        }`;
                    }

                    a.click();
                    window.URL.revokeObjectURL(url);
                    document.body.removeChild(a);

                    resolve(result);
                })
                .catch(err => {
                    console.error("Error, failed to download", err);
                    reject(err);
                });
        });
    }
}

export default Api;
