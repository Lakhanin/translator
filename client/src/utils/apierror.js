/**
 * @class APIError
 * @extends {Error}
 */
export class APIError extends Error {
    constructor(opts) {
        const { message, status, body } = opts || {};

        const errorMessage = body && body.message ? body.message : message;

        super(errorMessage);

        this.statusCode = status;
        this.response = body;
        this.isFunctional = status >= 400 && status < 500;
    }
}
