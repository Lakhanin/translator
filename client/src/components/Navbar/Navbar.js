import React from "react";
import LoginButton from "../Authorization/Login";
import LogoutButton from "../Authorization/Logout";
import styles from "./Navbar.module.css";
import { useAuth0 } from "@auth0/auth0-react";
import ModalContainer from "../Modal/ModalContainer";

function Navbar() {
    const { isAuthenticated } = useAuth0();
    return (
        <div className={styles.body}>
            <div className={styles.logo}>
                <img
                    src="https://cdn.worldvectorlogo.com/logos/aipn.svg"
                    alt=""
                />
                <div className={styles.logoName}>Translator</div>
            </div>

            <div className={styles.buttonsPanel}>
                <ModalContainer />
                <button>Change View</button>
                <div className={styles.buttonGreen}>
                    {isAuthenticated ? <LogoutButton /> : <LoginButton />}
                </div>
            </div>
        </div>
    );
}

export default Navbar;
