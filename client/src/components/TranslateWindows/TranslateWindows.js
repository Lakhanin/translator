import React from "react";
import styles from "./TranslateWindows.module.css";

const TranslateWindows = props => {
    const onChangeText = e => {
        const inputText = e.target.value;
        props.updateText(inputText);
    };

    const onChangeLanguage = e => {
        const changeLanguage = e.target.value;
        props.updateLanguage(changeLanguage);
    };

    return (
        <div className={styles.father}>
            <div className={styles.body}>
                <div className={styles.boxes}>
                    <select>
                        <option>Your language</option>
                    </select>
                    <textarea
                        onChange={onChangeText}
                        placeholder="Enter text"
                        id="text"
                        type="text"
                    />
                </div>
                <div className={styles.boxes}>
                    <select onChange={onChangeLanguage}>
                        <option disabled>Choise language</option>
                        <option value="ru">Russian</option>
                        <option value="en">English</option>
                        <option value="es">Spanish</option>
                        <option value="de">Deutsch</option>
                    </select>
                    <textarea
                        value={props.tanslatedText}
                        placeholder="You transleted text"
                        id="text"
                        type="text"
                    />
                </div>
            </div>
            <div className={styles.card}>
                <textarea
                    value={`${props.tanslatableText} - ${props.tanslatedText}`}
                    placeholder="Saved words"
                    id="text"
                    type="text"
                />
            </div>
        </div>
    );
};

export default TranslateWindows;
