import TranslateWindows from "./TranslateWindows";
import {
    addTextActionCreator,
    addLanguageActionCreator
} from "../../redux/actions/ui";
import { connect } from "react-redux";
import { compose } from "redux";

const mapStateToProps = state => {
    return {
        tanslatedText: state?.ui?.translate?.outputField,
        tanslatableText: state?.ui?.translate?.inputField
    };
};

const mapDispatchToProps = dispatch => {
    return {
        updateText: inputText => {
            dispatch(addTextActionCreator(inputText));
        },
        updateLanguage: changeLanguage => {
            dispatch(addLanguageActionCreator(changeLanguage));
        }
    };
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(
    TranslateWindows
);
