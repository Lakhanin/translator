import React, { useState, Fragment } from "react";
import Modal from "./Modal";

const ModalContainer = () => {
    const [open, setOpen] = useState(false);

    const openModal = () => {
        setOpen(true);
    };
    const handleSubmit = () => {
        console.log("Submit function!");
        setOpen(false);
    };
    const handleCancel = () => {
        console.log("Cancel function!");
        setOpen(false);
    };

    return (
        <Fragment>
            <button onClick={openModal}>Settings</button>
            <Modal
                title="Profile"
                isOpen={open}
                onCancel={handleCancel}
                onSubmit={handleSubmit}
            ></Modal>
        </Fragment>
    );
};

export default ModalContainer;
