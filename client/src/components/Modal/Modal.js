import React from "react";
import Profile from "../Profile/Profile";
import styles from "./Modal.module.css";

function Modal({ title, isOpen, onCancel, onSubmit }) {
    return (
        <>
            {isOpen && (
                <div className={styles.modalOverlay}>
                    <div className={styles.modalWindow}>
                        <div className={styles.modalHeader}>
                            <div className={styles.modalTitle}>{title}</div>
                        </div>
                        <div className={styles.modalBody}>
                            <Profile />
                        </div>
                        <div className={styles.modalFooter}>
                            <button onClick={onCancel}>Cancel</button>
                            <button onClick={onSubmit}>Submit</button>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
}

export default Modal;
