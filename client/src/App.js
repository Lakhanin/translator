import React from "react";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import { store } from "./redux/store";
import { history } from "./redux/enhancers/middlewares/router";
import Navbar from "./components/Navbar/Navbar";
import TranslateWindowsContainer from "./components/TranslateWindows/TranslateWindowsContainer";

const App = props => {
    return (
        <Provider store={store}>
            <ConnectedRouter history={history} />
            <Navbar />
            <TranslateWindowsContainer />
        </Provider>
    );
};

export default App;
