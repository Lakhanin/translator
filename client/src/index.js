import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Auth0Provider } from "@auth0/auth0-react";
import registerServiceWorker from "./utils/registerServiceWorker";

ReactDOM.render(
    <Auth0Provider
        domain="dev--uet481v.eu.auth0.com"
        clientId="GhL4eXPbgzuafC8q8IvSFb83f2Okw3V1"
        redirectUri={window.location.origin}
    >
        <App />
    </Auth0Provider>,
    document.getElementById("root")
);
registerServiceWorker();
