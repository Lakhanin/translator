import { put, call, select, takeLatest, all } from "redux-saga/effects";

//№2
import * as Fetch from "./fetch";
import * as Delete from "./delete";
import * as Post from "./post";

import * as Selectors from "../selectors";
import * as ActionTypes from "../actions/types";

export default function* rootSaga() {
    yield all([
        takeLatest(ActionTypes.ADD_TEXT, Fetch.translateLetter),
        takeLatest(ActionTypes.CHANGE_LANGUAGE, Fetch.translateLetter)
        // takeLatest(ActionTypes.ADD_TEXT, Fetch.test)
    ]);
}
