import { put, call, select } from "redux-saga/effects";
import * as Selectors from "../selectors";
import * as Services from "../services";
import * as Actions from "../actions/ui";

export function* translateLetter(action) {
    const word = yield action.text || select(Selectors.getWord);
    const lang = yield action.changeLanguage || select(Selectors.getLang);

    // const letter = yield select(Selectors.getLetter);
    console.log(action);
    try {
        const result = yield call(Services.fetchTranslate, word, lang);
        yield put(Actions.reciveTranslated(result)); //<= this is action
    } catch (e) {
        console.log(e.message);
    }
}
// export function* languageChange(action) {
//     const lang = action.changeLanguage;
//     // const letter = yield select(Selectors.getLetter);
//     console.log(action);
//     try {
//         yield call(Services.fetchLanguage, lang);
//     } catch (e) {
//         console.log(e.message);
//     }
// }

//№1
// export function* test(some Action) {
//     // const letter = yield select(Selectors.getLetter);

//     try {
//         yield call(Services.fetchTranslate);
//     } catch (e) {
//         console.log(e.message);
//     }
// }
// export function* channel(action) {
//     const token = yield select(Selectors.getToken),
//           agentId = yield select(Selectors.getActiveAgentId),
//           channelId = yield action.channelId || select(Selectors.getSelectedChannelId),
//           organisationId = yield select(Selectors.getActiveOrganisationId),
//           channelName = yield action.channelName

//     try {
//       if (!token || !organisationId || !channelId) {
//         throw new Error('No token, organisationId or channelId specified')
//       }

//       yield put(Actions.requestChannel())
//       const { payload } = yield call(Services.fetchChannel, token, organisationId, agentId, channelId, channelName)
//       yield put(Actions.receiveChannel(payload))

//     } catch (err) {

//       }))
//     }
//   }
