import * as ActionTypes from "./types";

export const openSidebar = () => ({
    type: ActionTypes.OPEN_SIDEBAR
});

export const closeSidebar = () => ({
    type: ActionTypes.CLOSE_SIDEBAR
});

export const addTextActionCreator = text => ({
    type: ActionTypes.ADD_TEXT,
    text
});
export const reciveTranslated = returnText => ({
    type: ActionTypes.RETURN_TEXT,
    returnText
});
export const addLanguageActionCreator = changeLanguage => ({
    type: ActionTypes.CHANGE_LANGUAGE,
    changeLanguage
});
