import { combineReducers } from "redux";

import sidebar from "./sidebar";
import translate from "./translate";

export default combineReducers({
    sidebar,
    translate
});
