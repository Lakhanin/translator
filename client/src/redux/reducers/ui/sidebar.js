import * as ActionTypes from "../../actions/types";

const initialState = {
    open: false
};

export default (state = initialState, { type }) => {
    switch (type) {
        case ActionTypes.OPEN_SIDEBAR:
            return {
                ...state,
                open: true
            };

        case ActionTypes.CLOSE_SIDEBAR:
            return {
                ...state,
                open: false
            };

        default:
            return state;
    }
};
