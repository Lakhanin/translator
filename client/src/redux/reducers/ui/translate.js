import * as ActionTypes from "../../actions/types";

const initialState = {
    inputField: "",
    outputField: ""
};
export default (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.ADD_TEXT: {
            return {
                ...state,
                inputField: action.text
            };
        }
        case ActionTypes.RETURN_TEXT: {
            return {
                ...state,
                outputField: action.returnText
            };
        }
        case ActionTypes.CHANGE_LANGUAGE: {
            return {
                ...state,
                language: action.changeLanguage
            };
        }
        default:
            return state;
    }
};
