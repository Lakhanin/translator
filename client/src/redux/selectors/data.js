export const getLang = state => state?.ui?.translate?.language;
export const getWord = state => state?.ui?.translate?.inputField;
