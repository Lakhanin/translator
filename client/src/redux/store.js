import { createStore, compose, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import reducer from "./reducers";
import enhancer from "./enhancers";

import rootSaga from "./sagas";

const sagaMiddleware = createSagaMiddleware({
    onError: err => {
        console.error("Uncaught error from Sagas", err, err.stack);
    }
});

const middleware = [sagaMiddleware];

export const store = createStore(
    reducer,
    {},
    compose(
        applyMiddleware(...middleware),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

sagaMiddleware.run(rootSaga);
