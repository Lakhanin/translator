import { Api } from "../utils/api";

const api = new Api(process.env.BACKEND_ENDPOINT || "");

export const fetchTranslate = (word, lang) =>
    api.post("translate", "", { word, lang });

// №3
// export const fetchTranslate = () => api.get("translate");
